using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using MyVod.Infrastructure.Models;

namespace MyVod.Areas.Admin.ViewModels;

public class MoviePageModel
{
    public MoviePageModel()
    {
        
    }
    
    [BindProperty]
    public Movie Current { get; set; }
    
    public List<SelectListItem> Persons { get; set; }
    public List<SelectListItem> Genres { get; set; }
    
    public Guid Director { get; set; }
    public Guid Genre { get; set; }
    public List<Guid> Writers { get; set; }
}