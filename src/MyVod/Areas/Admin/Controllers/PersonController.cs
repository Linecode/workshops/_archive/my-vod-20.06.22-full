using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MyVod.Infrastructure.Models;
using MyVod.Services;

namespace MyVod.Areas.Admin.Controllers;

[Authorize]
[Area("admin")]
public class PersonController : Controller
{
    private readonly IPersonService _personService;

    public PersonController(IPersonService personService)
    {
        _personService = personService;
    }

    [HttpGet]
    public async Task<IActionResult> Index()
    {
        var persons = _personService.Get();

        return View("Index", persons.AsEnumerable());
    }

    [HttpGet]
    [ActionName("create")]
    public IActionResult CreateView()
    {
        return View("Create");
    }

    [HttpPost]
    [ActionName("create")]
    public async Task<IActionResult> Create(Person genre)
    {
        if (ModelState.IsValid)
        {
            await _personService.Create(genre);
            
            return RedirectToAction(nameof(Index));
        }

        return BadRequest();
    }

    [HttpGet("{area}/{controller}/{action}/{id:guid}")]
    [ActionName("edit")]
    public async Task<IActionResult> EditView(Guid id)
    {
        var genre = await _personService.Get(id);

        if (genre is null)
            return NotFound();

        return View("Edit", genre);
    }

    [HttpPost("{area}/{controller}/{action}/{id:guid}")]
    [ActionName("edit")]
    public async Task<IActionResult> Edit(Guid id, Person genre)
    {
        if (id != genre.Id)
            return BadRequest();
            
        if (ModelState.IsValid)
        {
            await _personService.Update(genre);
            
            return RedirectToAction(nameof(Index));
        }

        return BadRequest();
    }

    [HttpGet("{area}/{controller}/{action}/{id:guid}/delete")]
    [ActionName("delete")]
    public async Task<IActionResult> Delete(Guid id)
    {
        if (ModelState.IsValid)
        {
            await _personService.Delete(id);
            
            return RedirectToAction(nameof(Index));
        }

        return BadRequest();
    }
}