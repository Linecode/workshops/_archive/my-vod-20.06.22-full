using System.Security.Claims;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MyVod.Domain.BusinessProcess.CreateBusinessSubscription;
using MyVod.Domain.CannonicalModel.PublishedLanguage;
using MyVod.Domain.Sales.B2b.Subscription.Application.Commands;
using MyVod.Infrastructure.Models;
using MyVod.Services;
using OpenSleigh.Core.Messaging;
using Stripe;
using Stripe.Checkout;
using Order = MyVod.Infrastructure.Models.Order;

namespace MyVod.Controllers;

public class OrderController : Controller
{
    private readonly IMovieService _movieService;
    private readonly IOrderService _orderService;
    private readonly ISender _sender;
    private readonly IMessageBus _messageBus;

    public OrderController(IMovieService movieService, IOrderService orderService, ISender sender, IMessageBus messageBus)
    {
        _movieService = movieService;
        _orderService = orderService;
        _sender = sender;
        _messageBus = messageBus;
    }
    
    [Authorize]
    [HttpPost("/order/{movieId:guid}")]
    public async Task<IActionResult> Index([FromRoute]Guid movieId, [FromForm]Order order)
    {
        order.MovieId = movieId;
        var movie = await _movieService.Get(movieId);
        order.MoviePrice = movie.Price;
        order.TaxRate = 23;
        order.Id = Guid.NewGuid();
        order.UserId = Guid.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value);

        await _orderService.Create(order);

        var price = movie.Price * ((double)order.TaxRate / 100.0 + 1.0);

        var checkout = await CheckoutSession.CreateCustomCheckoutSessionResponse(new CreateCustomCheckoutSessionRequest
        {
            Currency = "USD",
            Name = $"Film: {movie.Title}",
            Price = Convert.ToInt64(price),
            Quantity = 1,
            OrderId = order.Id
        });
        
        Response.Headers.Add("Location", checkout.RedirectUrl);
        return new StatusCodeResult(303);
    }

    [HttpGet("/order/process/success")]
    public async Task<IActionResult> Success()
    {
        return View();
    }

    [HttpGet("/order/process/error")]
    public async Task<IActionResult> Error()
    {
        return View();
    }

    [HttpPost("/order/webhook")]
    public async Task<IActionResult> WebHook()
    {
        var json = await new StreamReader(HttpContext.Request.Body).ReadToEndAsync();
        
        var stripeEvent = EventUtility.ConstructEvent(json,
            Request.Headers["Stripe-Signature"], "whsec_5c6c30132254d32b19b74dda9080e6138de4a745217f08897d98b26e5c2b4dd3");

        if (stripeEvent.Type == Events.CheckoutSessionCompleted)
        {
            var orderId = (stripeEvent.Data.Object as Session)?.ClientReferenceId ?? "";

            if (!string.IsNullOrWhiteSpace(orderId))
            {
                var id = Guid.Parse(orderId);
                var startSaga = new StartCreateBusinessSubscriptionSaga(Guid.NewGuid(), id, id,
                    new UserContext(Guid.NewGuid(), "some@email.com"));
                await _messageBus.PublishAsync(startSaga);
            }
        }

        if (stripeEvent.Type == Events.CheckoutSessionExpired)
        {
            var orderId = (stripeEvent.Data.Object as Session)?.ClientReferenceId ?? "";

            if (!string.IsNullOrWhiteSpace(orderId))
            {
                var result = await _sender.Send(new CancelOrderCommand(Guid.Parse(orderId)));
            }
        }
        
        return Ok();
    }
}