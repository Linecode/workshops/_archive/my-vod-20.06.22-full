using Microsoft.AspNetCore.Mvc;
using MyVod.Models;
using MyVod.Services;

namespace MyVod.Controllers;

public class CheckoutController : Controller
{
    private readonly IMovieService _movieService;

    public CheckoutController(IMovieService movieService)
    {
        _movieService = movieService;
    }
    
    [HttpGet("/checkout/{movieId:guid}")]
    public async Task<IActionResult> Index(Guid movieId)
    {
        var movie = await _movieService.Get(movieId);

        var checkoutModel = new CheckoutViewModel
        {
            Movie = movie,
            Tax = 23
        };
        
        return View(checkoutModel);
    }
}