using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MyVod.Services;

namespace MyVod.Controllers;

[Authorize]
public class HistoryController : Controller
{
    private readonly IOrderService _orderService;

    public HistoryController(IOrderService orderService)
    {
        _orderService = orderService;
    }
    
    [HttpGet("/history")]
    public async Task<IActionResult> Index()
    {
        var userId = Guid.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value);

        var orders = await _orderService.GetByUserId(userId);
        
        return View("Index", orders);
    }
}