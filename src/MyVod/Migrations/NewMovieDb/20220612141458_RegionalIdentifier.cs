﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MyVod.Migrations.NewMovieDb
{
    
    /// Small note:
    /// Run: dotnet ef migrations script --context NewMovieDbContext to check what SqlCode will be generated from this migration
    public partial class RegionalIdentifier : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<ushort>(
                name: "Region",
                table: "Movies",
                type: "INTEGER",
                nullable: false,
                defaultValue: (ushort)616);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Region",
                table: "Movies");
        }
    }
}
