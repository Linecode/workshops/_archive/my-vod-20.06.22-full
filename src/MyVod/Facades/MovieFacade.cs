using GeoFindService;
using MyVod.Domain.MoviesCatalog.ReadModel;
using MyVod.Domain.SharedKernel;

// Old stuff
using OldMovieService = MyVod.Services.IMovieService;
using OldMovieEntity = MyVod.Infrastructure.Models.Movie;
using OldPerson = MyVod.Infrastructure.Models.Person;

// New stuff
using NewMovieService = MyVod.Domain.MoviesCatalog.Application.IMovieService;
using MovieAggregate = MyVod.Domain.MoviesCatalog.Domain.Movie;

namespace MyVod.Facades;

public interface IMovieFacade
{
    Task Create(OldMovieEntity movie);
    Task Delete(OldMovieEntity movie);
    Task Delete(Guid id);
    Task Update(OldMovieEntity movie);
    Task<OldMovieEntity> Get(Guid id);
    IQueryable<OldMovieEntity> Get();
    Task<IEnumerable<OldMovieEntity>> GetLive();
}

public class MovieFacade : IMovieFacade
{
    private readonly OldMovieService _oldMovieService;
    private readonly NewMovieService _newMovieService;
    private readonly IGeoFindService _geoFindService;

    public MovieFacade(OldMovieService oldMovieService, NewMovieService newMovieService, IGeoFindService geoFindService)
    {
        _oldMovieService = oldMovieService;
        _newMovieService = newMovieService;
        _geoFindService = geoFindService;
    }
    
    public Task Create(OldMovieEntity movie)
    {
        return _oldMovieService.Create(movie);
    }

    public Task Delete(OldMovieEntity movie)
    {
        return _oldMovieService.Delete(movie);
    }

    public Task Delete(Guid id)
    {
        return _oldMovieService.Delete(id);
    }

    public Task Update(OldMovieEntity movie)
    {
        return _oldMovieService.Update(movie);
    }

    public async Task<OldMovieEntity> Get(Guid id)
    {
        var movie = await _newMovieService.Get(new MovieId(id));

        return OldNewMapper.NewToOld(movie);
    }

    public IQueryable<OldMovieEntity> Get()
    {
        return _oldMovieService.Get();
    }

    public async Task<IEnumerable<OldMovieEntity>> GetLive()
    {
        // return _oldMovieService.GetLive();

        var region = _geoFindService.GetUserLocation();
        var movies = await _newMovieService.GetAvailableMovies(region);

        return movies.Select(OldNewMapper.NewReadModelToOld);
    }

    private static class OldNewMapper
    {
        public static OldMovieEntity NewToOld(MovieAggregate movie) => new()
        {
            Cover = movie.Cover.ToString(),
            Trailer = movie.Trailer.ToString(),
            Description = movie.Description.Value,
            Id = movie.Id.Value,
            Title = movie.Title.Original,
            EnglishTitle = movie.Title.English,
            ReleasedAt = DateTime.UtcNow,
            Director = new OldPerson
            {
                Id = movie.Director.Id.Value,
                FirstName = movie.Director.FirstName,
                LastName = movie.Director.LastName,
            },
            DirectorId = movie.Director.Id.Value,
            Price = movie.Price.Value
        };

        public static OldMovieEntity NewReadModelToOld(MovieReadModel movie) => new()
        {
            Cover = movie.Cover.ToString(),
            Id = movie.Id.Value,
            Title = movie.Title.Original,
            EnglishTitle = movie.Title.English,
            Price = movie.Price.Value
        };
    }
}
