using MyVod.Domain.CannonicalModel.PublishedLanguage;

namespace MyVod.Domain.Sales.B2b.Subscription.Domain;

public static class OrderEvents
{
    public sealed class OrderCreated
    {
        public Guid OrderId { get; init; }
        public SubscriptionType SubscriptionType { get; init; }
    }

    public sealed class IntendAttached
    {
        public string Id { get; init; }
        public Uri RedirectUri { get; init; }
        public DateTime CreatedAt { get; init; }
    }

    public sealed class OrderCompleted
    {
        public DateTime When { get; init; }
    }

    public sealed class OrderCanceled
    {
        public DateTime When { get; init; }
    }
}