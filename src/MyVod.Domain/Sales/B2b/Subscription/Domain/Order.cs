using EnsureThat;
using MyVod.Common.BuildingBlocks.EventSourcing;
using MyVod.Domain.CannonicalModel.PublishedLanguage;

namespace MyVod.Domain.Sales.B2b.Subscription.Domain;

public class Order : AggregateBase
{
    public SubscriptionType SubscriptionType { get; private set; }
    public Intend Intend { get; private set;  }
    public OrderStatus Status { get; private set; } = OrderStatus.Created;

    public bool IsCompleted => Status == OrderStatus.Completed;
    public bool IsCanceled => Status == OrderStatus.Canceled;
    
    private Order(){}
    
    public Order(SubscriptionType subscriptionType)
    {
        Ensure.That(subscriptionType).IsNotNull();

        var orderId = Guid.NewGuid();
        
        var @event = new OrderEvents.OrderCreated { 
            OrderId = orderId, 
            SubscriptionType = subscriptionType
        };
        Apply(@event);
        AddUncommittedEvent(@event);
    }
    
    private void Apply(OrderEvents.OrderCreated @event)
    {
        Id = @event.OrderId;
        SubscriptionType = @event.SubscriptionType;

        Version++;
    }

    public void AttachIntend(PaymentIntend paymentIntend)
    {
        Ensure.That(paymentIntend.Id).IsNotEmptyOrWhiteSpace();
        Ensure.That(paymentIntend.RedirectUrl).IsNotNull();

        var @event = new OrderEvents.IntendAttached
        {
            Id = paymentIntend.Id,
            RedirectUri = paymentIntend.RedirectUrl,
            CreatedAt = DateTime.UtcNow
        };
        
        Apply(@event);
        AddUncommittedEvent(@event);
    }
    private void Apply(OrderEvents.IntendAttached @event)
    {
        var intend = new Intend(@event.Id, @event.RedirectUri, @event.CreatedAt);

        Intend = intend;

        Version++;
    }
    
    
    public void Complete()
    {
        if (Status == OrderStatus.Created)
        {
            var @event = new OrderEvents.OrderCompleted { When = DateTime.UtcNow };

            Apply(@event);
            AddUncommittedEvent(@event);
        }
    }

    private void Apply(OrderEvents.OrderCompleted _)
    {
        Status = OrderStatus.Completed;

        Version++;
    }

    public void Cancel()
    {
        if (Status == OrderStatus.Created)
        {
            var @event = new OrderEvents.OrderCanceled { When = DateTime.UtcNow };
            
            Apply(@event);
            AddUncommittedEvent(@event);
        }
    }

    private void Apply(OrderEvents.OrderCanceled _)
    {
        Status = OrderStatus.Canceled;

        Version++;
    }

    public enum OrderStatus
    {
        Created,
        Completed,
        Canceled
    }
}