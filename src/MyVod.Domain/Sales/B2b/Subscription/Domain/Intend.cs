using MyVod.Common.BuildingBlocks.Ddd;

namespace MyVod.Domain.Sales.B2b.Subscription.Domain;

public class Intend : ValueObject<Intend>
{
    public string Id { get; private set; }
    public Uri RedirectUri { get; private set; }
    public DateTime CreatedAt { get; private set; }

    public Intend(string id, Uri redirectUri, DateTime createdAt)
    {
        Id = id;
        RedirectUri = redirectUri;
        CreatedAt = createdAt;
    }

    protected override IEnumerable<object> GetEqualityComponents()
    {
        yield return Id;
        yield return RedirectUri;
        yield return CreatedAt;
    }
}
