using MyVod.Domain.CannonicalModel.PublishedLanguage;

namespace MyVod.Domain.Sales.B2b.Subscription.Application.Ports;

public interface IPaymentService
{
    Task<PaymentIntend> CreatePaymentIntendForSubscription(string name, long price, string currency, Guid orderId);
}
