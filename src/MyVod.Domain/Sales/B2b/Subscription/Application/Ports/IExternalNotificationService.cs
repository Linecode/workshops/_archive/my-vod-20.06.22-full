namespace MyVod.Domain.Sales.B2b.Subscription.Application.Ports;

public interface IExternalNotificationService
{
    Task<bool> SendNotification(string address, string text);
}