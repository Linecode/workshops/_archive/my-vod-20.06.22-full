using MediatR;
using MyVod.Common.BuildingBlocks.Common;
using MyVod.Common.BuildingBlocks.EventSourcing;

namespace MyVod.Domain.Sales.B2b.Subscription.Application.Commands;

public record GetSubscriptionQuery(Guid SubscriptionId) : IRequest<Result<Domain.Subscription>>;

internal class GetSubscriptionHandler : IRequestHandler<GetSubscriptionQuery, Result<Domain.Subscription>>
{
    private readonly IAggregateRepository _aggregateRepository;

    public GetSubscriptionHandler(IAggregateRepository aggregateRepository)
    {
        _aggregateRepository = aggregateRepository;
    }
    
    public async Task<Result<Domain.Subscription>> Handle(GetSubscriptionQuery request, CancellationToken cancellationToken)
    {
        var sub = await _aggregateRepository.Load<Domain.Subscription>(request.SubscriptionId,
            cancellationToken: cancellationToken);

        return Result<Domain.Subscription>.Success(sub);
    }
}