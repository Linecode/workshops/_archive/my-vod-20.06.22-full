using MediatR;
using MyVod.Common.BuildingBlocks.Common;
using MyVod.Common.BuildingBlocks.EventSourcing;
using MyVod.Domain.Sales.B2b.Subscription.Domain;

namespace MyVod.Domain.Sales.B2b.Subscription.Application.Commands;

public record CompleteOrderCommand(Guid OrderId) : IRequest<Result>;

public class CompleteOrderHandler : IRequestHandler<CompleteOrderCommand, Result>
{
    private readonly IAggregateRepository _repository;

    public CompleteOrderHandler(IAggregateRepository repository)
    {
        _repository = repository;
    }
    
    public async Task<Result> Handle(CompleteOrderCommand request, CancellationToken cancellationToken)
    {
        var order = await _repository.Load<Order>(request.OrderId, cancellationToken: cancellationToken);

        order.Complete();

        await _repository.Store(order, cancellationToken);
        
        return Result.Success();
    }
}