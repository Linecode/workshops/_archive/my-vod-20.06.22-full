using MediatR;
using MyVod.Common.BuildingBlocks.Common;
using MyVod.Common.BuildingBlocks.EventSourcing;
using MyVod.Domain.Sales.B2b.Subscription.Domain;

namespace MyVod.Domain.Sales.B2b.Subscription.Application.Commands;

public record CreateSubscriptionCommand(Guid OrderId) : IRequest<Result>;

internal class CreateSubscriptionHandler : IRequestHandler<CreateSubscriptionCommand, Result>
{
    private readonly IAggregateRepository _aggregateRepository;

    public CreateSubscriptionHandler(IAggregateRepository aggregateRepository)
    {
        _aggregateRepository = aggregateRepository;
    }

    public async Task<Result> Handle(CreateSubscriptionCommand request, CancellationToken cancellationToken)
    {
        var order = await _aggregateRepository.Load<Order>(request.OrderId, cancellationToken: cancellationToken);

        if (order is null)
            throw new Exception();

        if (!order.IsCompleted)
            throw new Exception();
        
        // Here we will need to pass user context from command
        var subscription = new Domain.Subscription(order.Id, order.SubscriptionType);

        await _aggregateRepository.Store(subscription, cancellationToken);
        
        return Result.Success();
    }
}