using MediatR;
using MyVod.Common.BuildingBlocks.Common;
using MyVod.Common.BuildingBlocks.EventSourcing;
using MyVod.Domain.Sales.B2b.Subscription.Domain;

namespace MyVod.Domain.Sales.B2b.Subscription.Application.Commands;

public record CancelOrderCommand(Guid OrderId) : IRequest<Result>;

internal class CancelOrderHandler : IRequestHandler<CancelOrderCommand, Result>
{
    private readonly IAggregateRepository _repository;

    public CancelOrderHandler(IAggregateRepository repository)
    {
        _repository = repository;
    }

    public async Task<Result> Handle(CancelOrderCommand request, CancellationToken cancellationToken)
    {
        var order = await _repository.Load<Order>(request.OrderId, cancellationToken: cancellationToken);

        if (order is null)
            throw new Exception();

        order.Cancel();

        await _repository.Store(order, cancellationToken);
        
        return Result.Success();
    }
}