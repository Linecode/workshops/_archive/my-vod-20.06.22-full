using MediatR;
using MyVod.Domain.Invoicing.Domain;
using MyVod.Domain.Invoicing.Infrastructure;

namespace MyVod.Domain.Invoicing.Application;

public record GetInvoiceQuery(InvoiceId id) : IRequest<Invoice>;

internal class GetInvoiceHandler : IRequestHandler<GetInvoiceQuery, Invoice>
{
    private readonly IInvoiceRepository _repository;

    public GetInvoiceHandler(IInvoiceRepository repository)
    {
        _repository = repository;
    }
    
    public async Task<Invoice> Handle(GetInvoiceQuery request, CancellationToken cancellationToken)
    {
        var invoice = await _repository.Get(request.id);

        return invoice;
    }
}
