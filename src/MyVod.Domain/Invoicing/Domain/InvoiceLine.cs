using MyVod.Common.BuildingBlocks.Ddd;

namespace MyVod.Domain.Invoicing.Domain;

public class InvoiceLine : ValueObject<InvoiceLine>
{
    public string Name { get; private set; }
    public int Quantity { get; private set; }
    public int Cost { get; private set; }

    public InvoiceLine(string name, int quantity, int cost)
    {
        Name = name;
        Quantity = quantity;
        Cost = cost;
    }

    protected override IEnumerable<object> GetEqualityComponents()
    {
        yield return Name;
        yield return Quantity;
        yield return Cost;
    }
}