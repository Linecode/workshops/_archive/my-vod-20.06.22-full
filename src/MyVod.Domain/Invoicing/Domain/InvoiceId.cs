using System.ComponentModel;
using System.Globalization;
using Newtonsoft.Json;

namespace MyVod.Domain.Invoicing.Domain;

[TypeConverter(typeof(InvoiceIdTypeConverter))]
[JsonConverter(typeof(InvoiceIdJsonConverter))]
public readonly struct InvoiceId : IComparable<InvoiceId>, IEquatable<InvoiceId>
{
    public Guid Value { get; }

    public static InvoiceId New() => new InvoiceId(Guid.NewGuid());

    public InvoiceId(Guid value) => Value = value;

    public bool Equals(InvoiceId other) => Value.Equals(other.Value);
    public override bool Equals(object obj) => obj is InvoiceId other && Equals(other);
    public int CompareTo(InvoiceId other) => Value.CompareTo(other.Value);
    public override int GetHashCode() => Value.GetHashCode();
    public override string ToString() => $"InvoiceId: {Value.ToString()}";
    public static bool operator ==(InvoiceId a, InvoiceId b) => a.CompareTo(b) == 0;
    public static bool operator !=(InvoiceId a, InvoiceId b) => !(a == b);

    private class InvoiceIdTypeConverter : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return sourceType == typeof(string) || base.CanConvertFrom(context, sourceType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            var stringValue = value as string;
            if (!string.IsNullOrEmpty(stringValue)
                && Guid.TryParse(stringValue, out var guid))
            {
                return new InvoiceId(guid);
            }

            return base.ConvertFrom(context, culture, value);
        }
    }

    private class InvoiceIdJsonConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(InvoiceId);
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var id = (InvoiceId)value;
            serializer.Serialize(writer, id.Value);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue,
            JsonSerializer serializer)
        {
            var guid = serializer.Deserialize<Guid>(reader);
            return new InvoiceId(guid);
        }
    }
}