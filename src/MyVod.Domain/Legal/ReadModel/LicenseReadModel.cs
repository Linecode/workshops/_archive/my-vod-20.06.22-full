using MyVod.Domain.Legal.Domain;
using MyVod.Domain.SharedKernel;

namespace MyVod.Domain.Legal.ReadModel;

public class LicenseReadModel
{
    public DateRange DateRange { get; private set; } = null!;
    public RegionIdentifier RegionIdentifier { get; private set; } = null!;
    public MovieId MovieId { get; private set; }
    public string Title { get; private set; } = null!;

    [Obsolete("Only for EF", true)]
    private LicenseReadModel()
    {
    }

    public LicenseReadModel(DateRange dateRange, RegionIdentifier regionIdentifier, MovieId movieId, string title)
    {
        DateRange = dateRange;
        RegionIdentifier = regionIdentifier;
        MovieId = movieId;
        Title = title;
    }

    public static LicenseReadModel From(License license, string title)
        => new(license.DateRange, license.RegionIdentifier, license.MovieId, title);
}