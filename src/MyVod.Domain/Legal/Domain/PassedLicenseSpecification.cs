using System.Linq.Expressions;
using MyVod.Common.BuildingBlocks.Ddd;

namespace MyVod.Domain.Legal.Domain;

public class PassedLicenseSpecification : Specification<License>
{
    public override Expression<Func<License, bool>> ToExpression()
        => license => license.DateRange.EndDate <= DateTime.UtcNow;
}