using MyVod.Common.BuildingBlocks.Ddd;
using MyVod.Common.BuildingBlocks.EFCore;
using MyVod.Domain.MoviesCatalog.Domain;
using MyVod.Domain.MoviesCatalog.ReadModel;
using MyVod.Domain.SharedKernel;

namespace MyVod.Domain.MoviesCatalog.Infrastructure;

public interface IMovieRepository : IRepository
{
    public IUnitOfWork UnitOfWork { get; }
    Task<Movie?> Get(MovieId id);
    Task<IEnumerable<Movie?>> Get(Specification<Movie> spec);
    
    // This could be done in separate repository(e.g. we could split repositories to read and write repository)
    Task<IEnumerable<MovieReadModel?>> GetReadModel(Specification<Movie> spec);
}