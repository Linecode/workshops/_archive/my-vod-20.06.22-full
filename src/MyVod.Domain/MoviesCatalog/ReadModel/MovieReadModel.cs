using MyVod.Domain.MoviesCatalog.Domain;
using MyVod.Domain.SharedKernel;

namespace MyVod.Domain.MoviesCatalog.ReadModel;

// We can have multiple read models
public class MovieReadModel
{
    public MovieId Id { get; private set; }
    public Title Title { get; private set; }
    public Uri Cover { get; private set; }
    public Money Price { get; private set; }

    [Obsolete("Only for EF", true)]
    private MovieReadModel()
    {
    }

    public MovieReadModel(MovieId id, Title title, Uri cover, Money price)
    {
        Id = id;
        Title = title;
        Cover = cover;
        Price = price;
    }

    public static MovieReadModel From(Movie movie) => new(movie.Id, movie.Title, movie.Cover, movie.Price);
}
