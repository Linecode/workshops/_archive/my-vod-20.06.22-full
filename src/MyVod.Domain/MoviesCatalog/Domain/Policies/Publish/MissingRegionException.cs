using System.Runtime.Serialization;

namespace MyVod.Domain.MoviesCatalog.Domain.Policies.Publish;

public class MissingRegionException : Exception
{
    public MissingRegionException()
    {
    }

    protected MissingRegionException(SerializationInfo info, StreamingContext context) : base(info, context)
    {
    }

    public MissingRegionException(string? message) : base(message)
    {
    }

    public MissingRegionException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}