namespace MyVod.Domain.CannonicalModel.PublishedLanguage;

public record UserContext(Guid UserId, string Email);
