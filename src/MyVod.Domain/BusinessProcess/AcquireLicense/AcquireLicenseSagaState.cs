using MyVod.Domain.Legal.Application;
using MyVod.Domain.Legal.Domain;
using MyVod.Domain.MoviesCatalog.Application;
using MyVod.Domain.SharedKernel;
using OpenSleigh.Core;
using OpenSleigh.Core.Messaging;

namespace MyVod.Domain.BusinessProcess.AcquireLicense;

public record AcquireLicenseSagaState : SagaState
{
    public AcquireLicenseSagaState(Guid id) : base(id)
    {
    }

    public LicenseId LicenseId { get; set; }
    public MovieId MovieId { get; set; }
    public RegionIdentifier RegionIdentifier { get; set; } = null!;
}

public record StartAcquireLicenseSaga(Guid Id, Guid CorrelationId, LicenseId LicenseId, MovieId MovieId,
    RegionIdentifier RegionIdentifier) : ICommand;
public record ActivateLicense(Guid Id, Guid CorrelationId) : ICommand;
public record PublishMovie(Guid Id, Guid CorrelationId) : ICommand;
public record AcquireLicenseSagaCompleted(Guid Id, Guid CorrelationId) : IEvent;

public class AcquireLicenseSaga : Saga<AcquireLicenseSagaState>,
    IStartedBy<StartAcquireLicenseSaga>,
    IHandleMessage<ActivateLicense>,
    IHandleMessage<PublishMovie>,
    IHandleMessage<AcquireLicenseSagaCompleted>
{
    private readonly ILicenseService _licenseService;
    private readonly IMovieService _movieService;

    public AcquireLicenseSaga(ILicenseService licenseService, IMovieService movieService, AcquireLicenseSagaState state) : base(state)
    {
        _licenseService = licenseService;
        _movieService = movieService;
    }

    public Task HandleAsync(IMessageContext<StartAcquireLicenseSaga> context,
        CancellationToken cancellationToken = default)
    {
        State.LicenseId = context.Message.LicenseId;
        State.MovieId = context.Message.MovieId;
        State.RegionIdentifier = context.Message.RegionIdentifier;
        
        Publish(new ActivateLicense(Guid.NewGuid(), context.Message.CorrelationId));
        
        return Task.CompletedTask;
    }

    public async Task HandleAsync(IMessageContext<ActivateLicense> context, CancellationToken cancellationToken = default)
    {
        await _licenseService.Activate(State.LicenseId);
        
        Publish(new PublishMovie(Guid.NewGuid(), context.Message.CorrelationId));
    }

    public async Task HandleAsync(IMessageContext<PublishMovie> context, CancellationToken cancellationToken = default)
    {
        await _movieService.Publish(State.MovieId, State.RegionIdentifier);
        
        Publish(new AcquireLicenseSagaCompleted(Guid.NewGuid(), context.Message.CorrelationId));
    }

    public Task HandleAsync(IMessageContext<AcquireLicenseSagaCompleted> context,
        CancellationToken cancellationToken = default)
    {
        State.MarkAsCompleted();

        return Task.CompletedTask;
    }
}