using MediatR;
using MyVod.Domain.CannonicalModel.PublishedLanguage;
using MyVod.Domain.Sales.B2b.Subscription.Application.Commands;
using OpenSleigh.Core;
using OpenSleigh.Core.Messaging;

namespace MyVod.Domain.BusinessProcess.CreateBusinessSubscription;

public record CreateBusinessSubscriptionSagaState : SagaState
{
    public CreateBusinessSubscriptionSagaState(Guid id) : base(id)
    {
    }

    public Guid OrderId { get; set; }
    public UserContext UserContext { get; set; }
}

public record StartCreateBusinessSubscriptionSaga
    (Guid Id, Guid CorrelationId, Guid OrderId, UserContext UserContext) : ICommand;

public record CompleteOrder(Guid Id, Guid CorrelationId) : ICommand;

public record CreateSubscription(Guid Id, Guid CorrelationId) : ICommand;

public record NotifyUser(Guid Id, Guid CorrelationId) : ICommand;

public record CompleteSaga(Guid Id, Guid CorrelationId) : ICommand;

public class CreateBusinessSubscriptionSaga : Saga<CreateBusinessSubscriptionSagaState>,
    IStartedBy<StartCreateBusinessSubscriptionSaga>,
    IHandleMessage<CompleteOrder>,
    IHandleMessage<CreateSubscription>,
    IHandleMessage<NotifyUser>,
    IHandleMessage<CompleteSaga>
{
    private readonly ISender _sender;

    public CreateBusinessSubscriptionSaga(CreateBusinessSubscriptionSagaState state, ISender sender) : base(state)
    {
        _sender = sender;
    }
    
    public Task HandleAsync(IMessageContext<StartCreateBusinessSubscriptionSaga> context, CancellationToken cancellationToken = new CancellationToken())
    {
        State.OrderId = context.Message.OrderId;
        State.UserContext = context.Message.UserContext;
        
        Publish(new CompleteOrder(Guid.NewGuid(), context.Message.CorrelationId));
        
        return Task.CompletedTask;
    }

    public async Task HandleAsync(IMessageContext<CompleteOrder> context, CancellationToken cancellationToken = new CancellationToken())
    {
        await _sender.Send(new CompleteOrderCommand(State.OrderId), cancellationToken);
        
        Publish(new CreateSubscription(Guid.NewGuid(), context.Message.CorrelationId));
    }

    public async Task HandleAsync(IMessageContext<CreateSubscription> context, CancellationToken cancellationToken = new CancellationToken())
    {
        await _sender.Send(new CreateSubscriptionCommand(State.OrderId), cancellationToken);
        
        Publish(new NotifyUser(Guid.NewGuid(), context.Message.CorrelationId));
    }
    
    // This could be listener on Subscription Created event but as we already have saga in place it is good fit here as well.
    public async Task HandleAsync(IMessageContext<NotifyUser> context, CancellationToken cancellationToken = new CancellationToken())
    {
        await _sender.Send(new NotifyUserCommand(State.UserContext));
        
        Publish(new CompleteSaga(Guid.NewGuid(), context.Message.CorrelationId));
    }

    public Task HandleAsync(IMessageContext<CompleteSaga> context, CancellationToken cancellationToken = new CancellationToken())
    {
        State.MarkAsCompleted();

        return Task.CompletedTask;
    }
}