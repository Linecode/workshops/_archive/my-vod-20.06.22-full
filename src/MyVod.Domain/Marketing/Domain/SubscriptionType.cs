using System.ComponentModel.DataAnnotations;

namespace MyVod.Domain.Marketing.Domain;

// This Domain is almost purely CRUD, we don't need DDD patterns here 
public class SubscriptionType
{
    public Guid Id { get; set; }
    
    [Required]
    public uint Price { get; set; }

    [Required]
    public TimeSpan TimeSpan { get; set; }
}