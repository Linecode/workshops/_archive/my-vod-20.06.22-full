using Stripe.Checkout;


namespace MyVod.Services;

public static class CheckoutSession
{
    private static readonly string SuccessUrl = "https://localhost:7220/order/process/success/";
    private static readonly string ErrorUrl = "https://localhost:7220/order/process/error/";

    public static async Task<CreateCheckoutSessionResponse> CreateCustomCheckoutSessionResponse(CreateCustomCheckoutSessionRequest request)
    {
        var options = new SessionCreateOptions
        {
            LineItems = new List<SessionLineItemOptions>
            {
                new()
                {
                    PriceData = new SessionLineItemPriceDataOptions
                    {
                        UnitAmount = request.Price,
                        Currency = request.Currency,
                        ProductData = new SessionLineItemPriceDataProductDataOptions
                        {
                            Name = request.Name,
                        },
                    },
                    Quantity = request.Quantity,
                },
            },
            Mode = "payment",
            SuccessUrl = SuccessUrl,
            CancelUrl = ErrorUrl,
            ClientReferenceId = request.OrderId.ToString()
        };

        var service = new SessionService();
        var session = await service.CreateAsync(options);
        
        var response = new CreateCheckoutSessionResponse()
        {
            SessionId = session.Id,
            RedirectUrl = session.Url
        };
        
        return response;
    }

    public static CreateCheckoutSessionResponse CreateCheckoutSessionResponse(CreateProductCheckoutSessionRequest request)
    {
        var options = new SessionCreateOptions
        {
            LineItems = new List<SessionLineItemOptions>
            {
                new()
                {
                    Price = request.PriceId,
                    Quantity = request.Quantity,
                },
            },
            Mode = "payment",
            SuccessUrl = SuccessUrl,
            CancelUrl = ErrorUrl,
        };

        var service = new SessionService();
        var session = service.Create(options);

        var response = new CreateCheckoutSessionResponse()
        {
            SessionId = session.Id,
            RedirectUrl = session.Url
        };
        
        return response;
    }
}

public class CreateCheckoutSessionResponse
{
    public string RedirectUrl { get; init; }

    public string SessionId { get; init; }
}

public class CreateCustomCheckoutSessionRequest
{
    public string Name { get; init; }
    
    public long Price { get; init; }

    public string Currency { get; init; }
    
    public int Quantity { get; init; }
    public Guid OrderId { get; set; }
}

public class CreateProductCheckoutSessionRequest
{
    public string PriceId { get; init; }
    
    public int Quantity { get; init; }
}
