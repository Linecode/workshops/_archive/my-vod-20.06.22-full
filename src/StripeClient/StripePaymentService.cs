using MyVod.Domain.CannonicalModel.PublishedLanguage;
using MyVod.Domain.Sales.B2b.Subscription.Application.Ports;

namespace StripeClient;

public class StripePaymentService : IPaymentService
{
    private readonly CheckoutSession _checkoutSession;

    public StripePaymentService(CheckoutSession checkoutSession)
    {
        _checkoutSession = checkoutSession;
    }

    public async Task<PaymentIntend> CreatePaymentIntendForSubscription(string name, long price, string currency, Guid orderId)
    {
        var request = new CreateCustomCheckoutSessionRequest
        {
            Currency = currency,
            Name = name,
            Price = price,
            Quantity = 1,
            OrderId = orderId
        };

        var response = await _checkoutSession.CreateCustomCheckoutSessionResponse(request);

        return new PaymentIntend(response.SessionId, new Uri(response.RedirectUrl));
    }
}