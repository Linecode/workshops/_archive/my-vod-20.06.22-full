using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyVod.Domain.MoviesCatalog.Domain;
using MyVod.Domain.SharedKernel;

namespace MyVod.Infrastructure.MoviesCatalog;

internal class MovieEntityTypeConfiguration : IEntityTypeConfiguration<Movie>
{
    public void Configure(EntityTypeBuilder<Movie> builder)
    {
        builder.HasKey(x => x.Id);

        builder.Property(x => x.Id)
            .HasConversion(id => id.Value, s => new MovieId(s));

        builder.Ignore(x => x.Timestamp);

        // builder.Property(x => x.Timestamp)
        //     .IsRowVersion();

        builder.Property(x => x.Cover)
            .HasConversion<string>();
        builder.Property(x => x.Trailer)
            .HasConversion<string>();
        builder.Property(x => x.Status);

        builder.Property(x => x.RegionIdentifier)
            .HasConversion(x => x.Value, u => new RegionIdentifier(u))
            .HasColumnName("Region");

        builder.OwnsOne(x => x.Price, p =>
        {
            p.Property(x => x.Value)
                .HasColumnName("Price");

            p.Ignore(x => x.Currency);
        });

        builder.OwnsOne(x => x.Title, t =>
        {
            t.Property(x => x.Original)
                .HasColumnName("Title");

            t.Property(x => x.English)
                .HasColumnName("EnglishTitle");
        });

        builder.OwnsOne(x => x.Description, d =>
        {
            d.Property(x => x.Value)
                .HasColumnName("Description");
        });

        builder.OwnsOne(x => x.MetaData, m =>
        {
            m.Property(x => x.Origin)
                .HasColumnName("Origin");

            m.Property(x => x.ReleasedAt)
                .HasColumnName("ReleasedAt");
        });

        builder.HasOne(x => x.Director);

        builder.ToTable("Movies");
    }
}