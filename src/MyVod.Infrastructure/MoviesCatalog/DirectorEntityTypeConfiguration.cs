using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyVod.Domain.MoviesCatalog.Domain;
using MyVod.Domain.SharedKernel;

namespace MyVod.Infrastructure.MoviesCatalog;

public class DirectorEntityTypeConfiguration : IEntityTypeConfiguration<Director>
{
    public void Configure(EntityTypeBuilder<Director> builder)
    {
        builder.HasKey(x => x.Id);

        builder.Property(x => x.Id)
            .HasConversion(x => x.Value, s => new DirectorId(s));

        builder.Property(x => x.FirstName);
        builder.Property(x => x.LastName);
        
        builder.ToTable("Persons");
    }
}