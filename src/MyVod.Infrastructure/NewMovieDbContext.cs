using Microsoft.EntityFrameworkCore;
using MyVod.Common.BuildingBlocks.EFCore;
using MyVod.Domain;
using MyVod.Domain.MoviesCatalog.Domain;
using MyVod.Infrastructure.MoviesCatalog;

namespace MyVod.Infrastructure;

public class NewMovieDbContext : DbContext, IUnitOfWork
{
    public DbSet<Movie?> Movies { get; private set; } = null!;
    
    public NewMovieDbContext(DbContextOptions<NewMovieDbContext> options) : base(options)
    {
        
    }

    public async Task<bool> SaveEntitiesAsync(CancellationToken cancellationToken = default)
    {
        await SaveChangesAsync(cancellationToken);

        return true;
    }
    
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        // modelBuilder.ApplyConfigurationsFromAssembly(typeof(NewMovieDbContext).Assembly);
        modelBuilder.ApplyConfiguration(new MovieEntityTypeConfiguration());
        modelBuilder.ApplyConfiguration(new DirectorEntityTypeConfiguration());
            
        base.OnModelCreating(modelBuilder);
    }
}