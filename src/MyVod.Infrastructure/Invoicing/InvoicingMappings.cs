using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MyVod.Common.BuildingBlocks.Ddd;
using MyVod.Domain.Invoicing.Domain;

namespace MyVod.Infrastructure.Invoicing;

public static class InvoicingMappings
{
    public static void RegisterMappers()
    {
        BsonClassMap.RegisterClassMap<Entity<InvoiceId>>(e =>
        {
            e.AutoMap();

            e.SetIdMember(e.GetMemberMap(x => x.Id).SetSerializer(new InvoiceIdSerializer()));

            e.UnmapMember(x => x.DomainEvents);
        });

        BsonClassMap.RegisterClassMap<Invoice>(im =>
        {
            im.MapProperty(x => x.From);
            im.MapProperty(x => x.To);
            im.MapProperty(x => x.CreatedAt);
            im.MapProperty(x => x.DueDate);
            im.MapProperty(x => x.Lines);
            im.MapProperty(x => x.Status)
                .SetSerializer(new EnumSerializer<InvoiceStatus>(BsonType.String));
        });
    }
}