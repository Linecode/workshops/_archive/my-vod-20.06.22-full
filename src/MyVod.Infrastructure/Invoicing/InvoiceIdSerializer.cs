using MongoDB.Bson.Serialization;
using MyVod.Domain.Invoicing.Domain;

namespace MyVod.Infrastructure.Invoicing;

public class InvoiceIdSerializer : IBsonSerializer<InvoiceId>
{
    object IBsonSerializer.Deserialize(BsonDeserializationContext context, BsonDeserializationArgs args)
    {
        var value = context.Reader.ReadString();
        return new InvoiceId(Guid.Parse(value));
    }

    public void Serialize(BsonSerializationContext context, BsonSerializationArgs args, InvoiceId value)
    {
        context.Writer.WriteString(value.Value.ToString());
    }

    public InvoiceId Deserialize(BsonDeserializationContext context, BsonDeserializationArgs args)
    {
        var value = context.Reader.ReadString();
        return new InvoiceId(Guid.Parse(value));
    }

    public void Serialize(BsonSerializationContext context, BsonSerializationArgs args, object value)
    {
        if (value is InvoiceId invoiceId)
            context.Writer.WriteString(invoiceId.Value.ToString());
        else
            throw new NotSupportedException($"This is not a {nameof(InvoiceId)}");
    }

    public Type ValueType => typeof(InvoiceId);
}