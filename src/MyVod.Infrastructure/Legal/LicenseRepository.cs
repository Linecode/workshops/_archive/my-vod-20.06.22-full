using Microsoft.EntityFrameworkCore;
using MyVod.Common.BuildingBlocks.Ddd;
using MyVod.Common.BuildingBlocks.EFCore;
using MyVod.Domain.Legal.Domain;
using MyVod.Domain.Legal.Infrastructure;

namespace MyVod.Infrastructure.Legal;

public class LicenseRepository : ILicenseRepository
{
    private readonly LegalDbContext _context;
    public IUnitOfWork UnitOfWork => _context;

    public LicenseRepository(LegalDbContext context)
    {
        _context = context;
    }

    public void Add(License license)
        => _context.Add(license);

    public ValueTask<License?> Get(LicenseId id)
        => _context.Licenses.FindAsync(id);

    public Task<List<License?>> Get(Specification<License> spec)
    {
        return _context.Licenses
            .Where(spec.ToExpression()!)
            .ToListAsync();
    }
}